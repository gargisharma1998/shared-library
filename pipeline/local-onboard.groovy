@Library('local-libs') _
    node {
        node('master') {
        stage('Cleaning Up Workspace'){
            utility.cleanWorkspace()   
        }
        }
        stage('Checkout repository') {
            node('master') {
            //https://gargisharma1998@bitbucket.org/gargisharma1998/shared-library.git
            env.ORG = "${params.REPO_TO_ADD}".split('/')[3]
            env.APP_NAME = "${params.REPO_TO_ADD}".split('/')[4].split('\\.')[0]
            if("${params.REPO_TO_ADD}".split('/')[2].contains('@')){
                env.REPO_URL = "${params.REPO_TO_ADD}".split('/')[2].split('@')[1]
            }
            else{
                env.REPO_URL = "${params.REPO_TO_ADD}".split('/')[2]
            }
            env.GIT_CRED_ID = 'BitBucketAdmin'
            echo "ORG: ${ORG}"
            echo "REPO_URL: ${REPO_URL}"
            echo "REPO_TO_ADD: ${params.REPO_TO_ADD}"
            echo "APP_NAME: ${APP_NAME}"
            checkout([$class: 'GitSCM', branches: [[name: "${BRANCH_TO_CONFIGURE}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: false, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: "${GIT_CRED_ID}", url: "https://${REPO_URL}/${ORG}/${APP_NAME}"]]])
        }
        }

        stage('Prepare workspace') {
            node('master') {
            preparefiles.create()
            sh """
              ls -ltr
            """
        }
        }
        stage('Create Jenkinsfile'){
            node('master') {
            utility.createJenkinsfile()
            }
        }
        stage('Commit the files to repo') {
            node('master') {
            withCredentials([usernamePassword(credentialsId: "${env.GIT_CRED_ID}", passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                sh """
                    git checkout ${BRANCH_TO_CONFIGURE}
                    git config --global credential.helper store
                    git config --global user.email \"gargisharma19021998@gmail.com\"
                    git config --global user.name \"gargisharma1998\"
                    git add . && git commit -a -m \"Added Jenkinsfile\" || :
                    git push https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/\${ORG}/\${APP_NAME} ${BRANCH_TO_CONFIGURE}
                """
            }
            }
        }

        stage('Create a job in Jenkins') {
            node('master') {
                withCredentials([usernamePassword(credentialsId: "${env.JKS_CRED_ID}", passwordVariable: 'JKS_PASSWORD', usernameVariable: 'JKS_USERNAME')]) {
                    sh """
                        sed -i 's/replace_the_repository/${APP_NAME}/g' template.xml 
                        java -jar ~/Downloads/jenkins-cli.jar -s http://localhost:9090 -auth ${JKS_USERNAME}:${JKS_PASSWORD} create-job multibranch/${APP_NAME} < template.xml
                        sleep 20
                    """
                }
            }
        }
   }

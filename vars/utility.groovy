//JsonBuilder is a builder for creating JSON payloads. 
import groovy.json.JsonBuilder  
//JSON slurper parses text or reader content into a data structure of lists and maps.
import groovy.json.JsonSlurper  

def cleanWorkspace()
    {
        echo "Cleaning up ${WORKSPACE}"
        // clean up our workspace 
        deleteDir()
        // clean up tmp directory 
        dir("${workspace}@tmp") {
            deleteDir()            
        }
    }
    
def createJenkinsfile(){
            writeFile file: 'Jenkinsfile', text: '@Library(\'local-libs\') _\ngradlebuild(deploy: true)'
        }
        
def getEnvironment(branchName)
    {   
        branchName = branchName.toLowerCase()
        if (branchName == 'master')
            return 'prod'
        else if (branchName == 'dev')
            return 'dev'
        else if (branchName == 'qa')
            return 'qa'
        else if (branchName == 'release')
            return 'qa'
        else
            return branchName
    }

def getAppName() {
        return "${scm.getUserRemoteConfigs()[0].getUrl()}".toString().split('/').last().split('\\.').first()
    }


def getOrgName(){
        //return "${scm.getUserRemoteConfigs()}".toString().split(' ')[2].split('/')[3]
        return "${scm.getUserRemoteConfigs()[0].getUrl()}".toString().split('/')[3]
}

def getRepoURL(){
        //return "${scm.getUserRemoteConfigs()}".toString().split(' ')[2].split('/')[2]
        return "${scm.getUserRemoteConfigs()[0].getUrl()}".toString()
}
  

   
def call(Map pipelineArgs){

pipeline {
   agent any
   stages {
     stage('Deploy on Dev') {
       when {  
           allOf { branch 'dev' }
       }
       steps{
        echo "This is from DEV BRANCH"
       }
     }
     stage('Deploy on QA') {
       when {  
           allOf { branch 'qa' }
       }
       steps{
         echo "This is fro QA BRANCH"
       }
     }
      stage('Deploy on Release') {
       when {  
           allOf { branch 'release' }
       }
       steps{
         echo "This is fro RELEASE BRANCH"
       }
     }
     stage('Deploy on master') {
       when {  
           allOf { branch 'master' }
       }
       steps {
          echo "This is from MASTER(PRODUCTION) branch"
       }
     }  
   }
 }
 }